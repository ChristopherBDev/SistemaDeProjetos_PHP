<?php

require_once 'Usuario.class.php';

class Aluno extends Usuario{

    private $numeroMaticula;

    public function registraProjeto(){

    }

    public function cadastrarSe(){

    }

    public function enviaComentario(){

    }

    public function avaliaProjeto(){

    }


    /**
     * Get the value of numeroMaticula
     */ 
    public function getNumeroMaticula()
    {
        return $this->numeroMaticula;
    }

    /**
     * Set the value of numeroMaticula
     *
     * @return  self
     */ 
    public function setNumeroMaticula($numeroMaticula)
    {
        $this->numeroMaticula = $numeroMaticula;

        return $this;
    }
}

?>