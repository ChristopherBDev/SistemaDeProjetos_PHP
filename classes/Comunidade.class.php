<?php

class Comunidade extends Categoria {

    private $publicoAlvo;



    /**
     * Get the value of publicoAlvo
     */ 
    public function getPublicoAlvo()
    {
        return $this->publicoAlvo;
    }

    /**
     * Set the value of publicoAlvo
     *
     * @return  self
     */ 
    public function setPublicoAlvo($publicoAlvo)
    {
        $this->publicoAlvo = $publicoAlvo;

        return $this;
    }
}

?>