<?php

class Intitucional extends Categoria {

    private $departamento;
    private $resultados;



    /**
     * Get the value of departamento
     */ 
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set the value of departamento
     *
     * @return  self
     */ 
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get the value of resultados
     */ 
    public function getResultados()
    {
        return $this->resultados;
    }

    /**
     * Set the value of resultados
     *
     * @return  self
     */ 
    public function setResultados($resultados)
    {
        $this->resultados = $resultados;

        return $this;
    }
}

?>