<?php

class MercadoDeTrabalho extends Categoria {

    private $areaDeAtuacao;



    /**
     * Get the value of areaDeAtuacao
     */ 
    public function getAreaDeAtuacao()
    {
        return $this->areaDeAtuacao;
    }

    /**
     * Set the value of areaDeAtuacao
     *
     * @return  self
     */ 
    public function setAreaDeAtuacao($areaDeAtuacao)
    {
        $this->areaDeAtuacao = $areaDeAtuacao;

        return $this;
    }
}

?>