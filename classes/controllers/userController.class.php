<?php

require_once __DIR__ . '/../Professor.class.php';
require_once __DIR__ . '/../Aluno.class.php';
require_once __DIR__ . '/../banco/DB.php';
require_once __DIR__ . '/../../interfaces/IController.php';

class UserController implements IController
{

    public function create($user): bool
    {
        $db = DB::criaBanco();

        if ($user instanceof Professor) {
            $nomeCompleto = $user->getNomeCompleto();
            $email = $user->getEmail();
            $senha = $user->getSenha();
            $tipo = 'Professor';
            $SIAPE = $user->getSIAPE();
            $numeroMatricula = null;
        } else if ($user instanceof Aluno) {
            $nomeCompleto = $user->getNomeCompleto();
            $email = $user->getEmail();
            $senha = $user->getSenha();
            $tipo = 'Aluno';
            $SIAPE = null;
            $numeroMatricula = $user->getNumeroMaticula();
        }

        $sql = "INSERT INTO usuario (nomeCompleto, email, senha, tipo, SIAPE, numeroMatricula) VALUES ('$nomeCompleto', '$email', '$senha', '$tipo', '$SIAPE', '$numeroMatricula');";

        try {
            $response = $db->prepare($sql);
            $response->execute();
            return true;
        } catch (Exception $ex) {
            return new Exception("Erro ao tentar criar um user: $ex", 1);
        }

    }

    public function read(): array
    {
        $db = DB::criaBanco();

        $select = "SELECT * FROM usuario";
        try {
            $result = $db->prepare($select);
            $result->execute();
            $listaUsers = $result->fetchAll(PDO::FETCH_ASSOC);
            return $listaUsers;
        } catch (Exception $ex) {
            throw new Exception($e, 1);
        }

    }

    public function getById($user = null, $id = 0)
    {
        $db = DB::criaBanco();

        if ($id != 0) {
            $select = "SELECT * FROM usuario WHERE userID='$id'";
        } else if ($user instanceof Professor) {
            $select = "SELECT * FROM usuario WHERE SIAPE='" . $user->getSIAPE() . "' AND senha='" . $user->getSenha()."'";
        } else if ($user instanceof Aluno) {
            $select = "SELECT * FROM usuario WHERE numeroMatricula='" . $user->getNumeroMaticula() . "' AND senha='" . $user->getSenha()."'";
        }

        try {
            $result = $db->prepare($select);
            $result->execute();
            $usuario = $result->fetchAll(PDO::FETCH_ASSOC);
            return $usuario;
        } catch (Exception $ex) {
            return new Exception("Erro ao tentar buscar user: $ex");
        }

    }

    public function update($user): bool
    {
        $db = DB::criaBanco();

        if ($user instanceof Professor || $user instanceof Aluno) {
            $id = $user->getUserID();
            $nomeCompleto = $user->getNomeCompleto();
            $email = $user->getEmail();
            $senha = $user->getSenha();
            $SIAPE = $user instanceof Professor ? $user->getSIAPE() : 0;
            $numeroMatricula = $user instanceof Aluno ? $user->getNumeroMaticula() : 0;
        }

        $select = "UPDATE usuario SET
            userID=$id,
            nomeCompleto='$nomeCompleto',
            email='$email',
            senha='$senha',
            SIAPE=$SIAPE,
            numeroMatricula=$numeroMatricula
             WHERE userID=$id";

        try
        {
            $result = $db->prepare($select);
            $result->execute();
            // $result->fetchAll(PDO::FETCH_ASSOC);
            return true;
        } catch (Exception $ex) {
            return false;
            throw new Exception("Erro: $ex");
        }

        return false;
    }

    public function delete($id): bool
    {
        $db = DB::criaBanco();

        $select = "DELETE FROM usuario WHERE userID=$id";
        try {
            $result = $db->prepare($select);
            $result->execute();
            // $result->fetchAll(PDO::FETCH_ASSOC);
            return true;
        } catch (Exception $ex) {
            return false;
            throw new Exception("Erro: $ex");
        }

    }

}
