<?php

require_once 'Usuario.class.php';

class Professor extends Usuario{

    private $SIAPE;

    public function cadastrarSe(){

    }

    public function enviaComentario(){

    }

    public function avaliaProjeto(){

    }


    /**
     * Get the value of SIAPE
     */ 
    public function getSIAPE()
    {
        return $this->SIAPE;
    }

    /**
     * Set the value of SIAPE
     *
     * @return  self
     */ 
    public function setSIAPE($SIAPE)
    {
        $this->SIAPE = $SIAPE;

        return $this;
    }
}

?>