<?php

class Projeto
{
    private $titulo;
    private $resumo;
    private $tecnologias;
    private $status;
    private $dataCadastro;
    private $dataUltimaAlteracao;
    private $mesesDuracao;
    private $colaboradoresID;
    private $coordenadorID;
    private $comentariosID;
    private $periodoVotacao;

    function inscreverEmConcurso(){

    }


    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of resumo
     */ 
    public function getResumo()
    {
        return $this->resumo;
    }

    /**
     * Set the value of resumo
     *
     * @return  self
     */ 
    public function setResumo($resumo)
    {
        $this->resumo = $resumo;

        return $this;
    }

    /**
     * Get the value of tecnologias
     */ 
    public function getTecnologias()
    {
        return $this->tecnologias;
    }

    /**
     * Set the value of tecnologias
     *
     * @return  self
     */ 
    public function setTecnologias($tecnologias)
    {
        $this->tecnologias = $tecnologias;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of dataCadastro
     */ 
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set the value of dataCadastro
     *
     * @return  self
     */ 
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;

        return $this;
    }

    /**
     * Get the value of periodoVotacao
     */ 
    public function getPeriodoVotacao()
    {
        return $this->periodoVotacao;
    }

    /**
     * Set the value of periodoVotacao
     *
     * @return  self
     */ 
    public function setPeriodoVotacao($periodoVotacao)
    {
        $this->periodoVotacao = $periodoVotacao;

        return $this;
    }

    /**
     * Get the value of dataUltimaAlteracao
     */ 
    public function getDataUltimaAlteracao()
    {
        return $this->dataUltimaAlteracao;
    }

    /**
     * Set the value of dataUltimaAlteracao
     *
     * @return  self
     */ 
    public function setDataUltimaAlteracao($dataUltimaAlteracao)
    {
        $this->dataUltimaAlteracao = $dataUltimaAlteracao;

        return $this;
    }

    /**
     * Get the value of mesesDuracao
     */ 
    public function getMesesDuracao()
    {
        return $this->mesesDuracao;
    }

    /**
     * Set the value of mesesDuracao
     *
     * @return  self
     */ 
    public function setMesesDuracao($mesesDuracao)
    {
        $this->mesesDuracao = $mesesDuracao;

        return $this;
    }

    /**
     * Get the value of colaboradoresID
     */ 
    public function getColaboradoresID()
    {
        return $this->colaboradoresID;
    }

    /**
     * Set the value of colaboradoresID
     *
     * @return  self
     */ 
    public function setColaboradoresID($colaboradoresID)
    {
        $this->colaboradoresID = $colaboradoresID;

        return $this;
    }

    /**
     * Get the value of coordenadorID
     */ 
    public function getCoordenadorID()
    {
        return $this->coordenadorID;
    }

    /**
     * Set the value of coordenadorID
     *
     * @return  self
     */ 
    public function setCoordenadorID($coordenadorID)
    {
        $this->coordenadorID = $coordenadorID;

        return $this;
    }

    /**
     * Get the value of comentariosID
     */ 
    public function getComentariosID()
    {
        return $this->comentariosID;
    }

    /**
     * Set the value of comentariosID
     *
     * @return  self
     */ 
    public function setComentariosID($comentariosID)
    {
        $this->comentariosID = $comentariosID;

        return $this;
    }
}
