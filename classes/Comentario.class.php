<?php

class Comentario {

    private $dataHoraRegistro;
    private $comentario;
    private $usuarioComentario;
    private $resposta;


    /**
     * Get the value of dataHoraRegistro
     */ 
    public function getDataHoraRegistro()
    {
        return $this->dataHoraRegistro;
    }

    /**
     * Set the value of dataHoraRegistro
     *
     * @return  self
     */ 
    public function setDataHoraRegistro($dataHoraRegistro)
    {
        $this->dataHoraRegistro = $dataHoraRegistro;

        return $this;
    }

    /**
     * Get the value of comentario
     */ 
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set the value of comentario
     *
     * @return  self
     */ 
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get the value of usuarioComentario
     */ 
    public function getUsuarioComentario()
    {
        return $this->usuarioComentario;
    }

    /**
     * Set the value of usuarioComentario
     *
     * @return  self
     */ 
    public function setUsuarioComentario($usuarioComentario)
    {
        $this->usuarioComentario = $usuarioComentario;

        return $this;
    }

    /**
     * Get the value of resposta
     */ 
    public function getResposta()
    {
        return $this->resposta;
    }

    /**
     * Set the value of resposta
     *
     * @return  self
     */ 
    public function setResposta($resposta)
    {
        $this->resposta = $resposta;

        return $this;
    }

}