<?php

class Concurso{

    private $titulo;
    private $decricao;
    private $periodoInscricao;
    private $projetosInscritosID;
    private $categoriasID;
    private $dataPremiacao;
    private $descPremiacao;
    private $tipoAvaliacao;

    

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of decricao
     */ 
    public function getDecricao()
    {
        return $this->decricao;
    }

    /**
     * Set the value of decricao
     *
     * @return  self
     */ 
    public function setDecricao($decricao)
    {
        $this->decricao = $decricao;

        return $this;
    }

    /**
     * Get the value of periodoInscricao
     */ 
    public function getPeriodoInscricao()
    {
        return $this->periodoInscricao;
    }

    /**
     * Set the value of periodoInscricao
     *
     * @return  self
     */ 
    public function setPeriodoInscricao($periodoInscricao)
    {
        $this->periodoInscricao = $periodoInscricao;

        return $this;
    }


    /**
     * Get the value of dataPremiacao
     */ 
    public function getDataPremiacao()
    {
        return $this->dataPremiacao;
    }

    /**
     * Set the value of dataPremiacao
     *
     * @return  self
     */ 
    public function setDataPremiacao($dataPremiacao)
    {
        $this->dataPremiacao = $dataPremiacao;

        return $this;
    }

    /**
     * Get the value of descPremiacao
     */ 
    public function getDescPremiacao()
    {
        return $this->descPremiacao;
    }

    /**
     * Set the value of descPremiacao
     *
     * @return  self
     */ 
    public function setDescPremiacao($descPremiacao)
    {
        $this->descPremiacao = $descPremiacao;

        return $this;
    }

    /**
     * Get the value of tipoAvaliacao
     */ 
    public function getTipoAvaliacao()
    {
        return $this->tipoAvaliacao;
    }

    /**
     * Set the value of tipoAvaliacao
     *
     * @return  self
     */ 
    public function setTipoAvaliacao($tipoAvaliacao)
    {
        $this->tipoAvaliacao = $tipoAvaliacao;

        return $this;
    }

    /**
     * Get the value of projetosInscritosID
     */ 
    public function getProjetosInscritosID()
    {
        return $this->projetosInscritosID;
    }

    /**
     * Set the value of projetosInscritosID
     *
     * @return  self
     */ 
    public function setProjetosInscritosID($projetosInscritosID)
    {
        $this->projetosInscritosID = $projetosInscritosID;

        return $this;
    }

    /**
     * Get the value of categoriasID
     */ 
    public function getCategoriasID()
    {
        return $this->categoriasID;
    }

    /**
     * Set the value of categoriasID
     *
     * @return  self
     */ 
    public function setCategoriasID($categoriasID)
    {
        $this->categoriasID = $categoriasID;

        return $this;
    }
}

?>