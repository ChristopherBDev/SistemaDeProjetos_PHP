<?php

class DB
{
    public static $host = 'localhost';
    public static $dbname = 'projetos';
    public static $user = 'root';
    public static $pass = '';

    public static function criaBanco(): PDO{
        
        $db = new PDO(
            'mysql:host=' . self::$host . ';dbname=' . self::$dbname . ';charset=utf8',
            self::$user, self::$pass);

        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Caso a conexão seja reprovada, exibe na tela uma mensagem de erro
        if (!$db) {
            die("<h1>Falha na conexão com o Banco de Dados!</h1>");
        }

        return $db;
    }

}

//conexão com o servidor
// $conect = mysqli_connect("localhost", "root", "", "projetos");

/*Configurando este arquivo, depois é só você dar um include em suas paginas php,
isto facilita muito, pois caso haja necessidade de mudar seu Banco de Dados
você altera somente um arquivo*/
