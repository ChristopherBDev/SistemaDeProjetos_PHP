//===========================>indexLogin e cadastro
$(document).ready(function () {

    defineCampoPersonalizado();

    $("#tipo").change(function () {

        defineCampoPersonalizado();
    });


    function defineCampoPersonalizado() {

        campoPersonalizado = $('#tipo').val() == 'professor' ? 'SIAPE' : 'numeroMatricula';

        $('label.campoPersonalizado').text(campoPersonalizado);
        $('input.campoPersonalizado').attr('name', campoPersonalizado);
        $('input.campoPersonalizado').attr('id', campoPersonalizado);
    }




});
//indexLogin e cadastro<===========================

//===========================>cadastro
//-----cadastrar-----
function validaUser() {
    var user = new Object();
    user.nomeCompleto = $('#nomeCompleto').val();
    user.email = $('#email').val();
    user.tipo = $('#tipo').val();
    user.senha = $('#senha').val();
    user.SIAPE = $('#SIAPE').val();
    user.numeroMatricula = $('#numeroMatricula').val();

    //valida os dados do user aqui

    //se tudo estiver certo, vai para:
    cadastraUser(user);
}

function cadastraUser(user) {

    $.post("http://localhost:80/projetosPHP/cadastro.php", {
        User: user,
        Acao: 'Cadastrar'
    }, function (response) {
        var resposta = JSON.parse(response);

        if (resposta[0] == "True") {
            alert(resposta[1]);
            window.location.href = 'login-page.php';
        }
        else {
            console.error(resposta[1]);
        }

    });
}

//-----atualizar-----
function validaUserAtualizado() {
    var user = new Object();
    user.userID = $('#userID').val();
    user.nomeCompleto = $('#nomeCompleto').val();
    user.email = $('#email').val();
    user.tipo = $('#tipo').val();
    user.senha = $('#senha').val();
    user.SIAPE = $('#SIAPE').val();
    user.numeroMatricula = $('#numeroMatricula').val();

    //valida os dados do user aqui

    //se tudo estiver certo, vai para:
    atualizaUser(user);
}

function atualizaUser(user) {

    $.post("http://localhost:80/projetosPHP/cadastro.php", {
        User: user,
        Acao: 'Atualizar'
    }, function (response) {
        var resposta = JSON.parse(response);

        if (resposta[0] == "true") {
            alert(resposta[1]);
        }
        else {
            console.error(resposta[1]);
            alert(resposta[1])
        }

    });
}

//------Excluir------
function apagarUser() {
    var confirm = window.confirm("Deseja realmente excluir sua conta?");
    if (confirm) {
        var userID = $('#userID').val();

        apagaConta(userID);
    }
    else {
        //do nothing
    }

}

function apagaConta(userID) {

    $.post("http://localhost:80/projetosPHP/deleteUser.php", {
        UserID: userID
    }, function (response) {
        var resposta = JSON.parse(response);

        if (resposta[0] == "true") {
            alert(resposta[1]);
            window.location.href = 'logout.php';//apaga sessão e retorna a Home
        }
        else {
            console.error(resposta[1]);
            alert(resposta[1])
        }

    });
}
//cadastro<===========================

//===========================>login-page.php
function validaLogin() {
    var mensagemValidacao = "";
    var user = new Object();
    var tipo = $('#tipo').val();

    if (tipo == 'professor') {
        user.SIAPE = $('#SIAPE').val();
        if (user.SIAPE == undefined || user.SIAPE == "") {
            mensagemValidacao += ('Campo de SIAPE vazio.\n');
        }
    }
    else if (tipo == 'aluno') {
        user.numeroMatricula = $('#numeroMatricula').val();
        if (user.numeroMatricula == undefined || user.numeroMatricula == "") {
            mensagemValidacao += ('Campo de Número de Matricula vazio.\n');
        }
    }
    user.senha = $('#senha').val();
    if (user.senha == undefined || user.senha == "") {
        mensagemValidacao += ('Campo de Senha vazio.');
    }

    if (mensagemValidacao != "") {
        alert(mensagemValidacao);
    }
    else {
        login(user);
    }

}

function login(user) {
    var tipo = $('#tipo').val();

    $.post("http://localhost:80/projetosPHP/login.php", {
        Tipo: tipo,
        User: user
    }, function (response) {

        if (JSON.parse(response)[0] == "True") {
            // var resposta = JSON.parse(response);

            // console.log(resposta[1]);
            // user = JSON.stringify(resposta[1]);
            // sessionStorage.setItem('user', user);
            window.location.href = 'home-page.php';
        }
        else if (JSON.parse(response)[0] == "False") {
            var resposta = JSON.parse(response);
            console.error(resposta[1]);
            alert(resposta[1]);
        }

    });
}
//login-page.php<===========================

//===========================>home-page.php
function goToPerfil() {
    window.location.href = 'cadastro-page.php';
}

function goToLogin() {
    window.location.href = 'login-page.php';
}

function goToCadastro() {
    window.location.href = 'cadastro-page.php';
}
//home-page.php<===========================