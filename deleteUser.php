<?php

require_once __DIR__ . '/classes/controllers/userController.class.php';

if (!isset($_POST)) {
    throw new Exception("Nada passado por Post.", 1);
    die;
}

$userID = $_POST['UserID'];

$userController = new UserController();

$response = array();

try {
    $apagou = $userController->delete($userID);

    if ($apagou) {
        $response[0] = "True";
        $response[1] = "Conta excluída com sucesso!";
        echo json_encode($response);
    } else {
        $response[0] = "False";
        $response[1] = "Falha ao tentar excluir a conta!";
        echo json_encode($response);
    }

} catch (Exception $ex) {
    throw new Exception("Erro ao tentar excluir o user: $ex", 1);

    die;
}
