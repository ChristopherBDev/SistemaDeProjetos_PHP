<?php

require_once __DIR__ . '/classes/Professor.class.php';
require_once __DIR__ . '/classes/Aluno.class.php';
require_once __DIR__ . '/classes/controllers/userController.class.php';

if (!isset($_POST)) {
    throw new Exception("Nada passado por Post.", 1);
    die;
}

$tipo = $_POST['Tipo'];
$user = $_POST['User'];

$response = array();

$userController = new UserController();

if ($tipo == 'professor') {
    $professor = new Professor();
    $professor->setSIAPE($user['SIAPE']);
    $professor->setSenha($user['senha']);
    $objectResponse = $userController->getById($professor);
    if (count($objectResponse) > 0) {
        session_start();
        $_SESSION['user'] = $objectResponse[0];
        $response[0] = "True";
        echo json_encode($response);
    } else {
        $response[0] = "False";
        $response[1] = "Credenciais incorretas.";
        echo json_encode($response);
    }

} else if ($tipo == 'aluno') {
    $aluno = new Aluno();
    $aluno->setNumeroMaticula($user['numeroMatricula']);
    $aluno->setSenha($user['senha']);
    $objectResponse = $userController->getById($aluno);
    if (count($objectResponse) > 0) {
        session_start();
        $_SESSION['user'] = $objectResponse[0];
        $response[0] = "True";
        echo json_encode($response);

    } else {
        $response[0] = "False";
        $response[1] = "Credenciais incorretas.";
        echo json_encode($response);
    }
}
