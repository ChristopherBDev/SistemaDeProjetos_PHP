<?php

interface IController {

    function create($object);
    function read();
    function getById($object = null, $id=0);//pois usa o mesmo método para login e para trazer os dados de uma tabela pelo ID
    function update($object);
    function delete($id);

}

?>