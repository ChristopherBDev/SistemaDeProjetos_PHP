<?php

session_start();

$user = null;
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Página inicial</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <?php
include_once 'header.html';
?>
<script>
    function logout(){
        window.location.href="../logout.php";
    }
</script>
</head>
<body>
<?php
if (isset($user)):
?>
    <h1> Bem vindo, <?php echo $user['tipo'] . " " . $user['nomeCompleto']; ?></h1>
    <hr>
    <button onclick="goToPerfil(); return false;" id="perfil" name="perfil" >Perfil</button>
    <button onclick="logout();" id="logout" name="logout" >Sair</button>
<?php
elseif (!isset($user)):
?>
    <button onclick="goToLogin(); return false;" id="login" name="login" >Login</button>
    <button onclick="goToCadastro(); return false;" id="cadastro" name="cadastro" >Criar Conta</button>
    <?php
endif;
?>
</body>
</html>
