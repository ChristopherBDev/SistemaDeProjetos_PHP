<?php

session_start();

if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title> Cadastro de Usuário </title>
        <?php
include_once 'header.html';

?>
        <style>
            .nav{
                text-align: center;
            }
            #userID{
                display:none;
            }
        </style>
    </head>
    <body>
        <div class="nav"  >
            <form class="form-cadastro" action="#" method="post">
                <input type="number" display="none" name="userID" id="userID" value="<?php if(isset($user)) : echo $user['userID']; endif; ?>"><br>
                <label>Nome Completo:</label><br>
                <input type="text" name="nomeCompleto" id="nomeCompleto" value="<?php if(isset($user)) : echo $user['nomeCompleto']; endif; ?>"><br>
                <label>Email:</label><br>
                <input type="email" name="email" id="email" value="<?php if(isset($user)) : echo $user['email']; endif; ?>"><br>
                <label>Tipo:</label><br>
                <select id="tipo" name="tipo" <?php echo isset($user) ? "disabled='true'" : "" ?> >
                    <option value="professor" selected="<?php if(isset($user) && $user['tipo'] == 'Professor'): echo 'selected'; endif; ?>">Professor</option>
                    <option value="aluno" selected="<?php if(isset($user) && $user['tipo'] == 'Aluno'): echo 'selected'; endif; ?>">Aluno</option>
                </select><br>
                <!-- campo de SIAPE ou numeroMatricula abaixo -->
                <label class="campoPersonalizado"></label><br>
                <input type="number" class="campoPersonalizado" <?php echo isset($user) ? "disabled='true'" : "" ?> value="<?php if(isset($user)) : echo $user['tipo'] == 'Professor' ? $user['SIAPE'] : $user['numeroMatricula'] ; endif; ?>"/>
                <br>
                <label>Senha:</label><br>
                <input type="password" name="senha" id="senha" value="<?php if(isset($user)) : echo $user['senha']; endif; ?>"><br>

                <hr>
                <?php
                if (!isset($user)):
                ?>
                <button onclick="validaUser(); return false;" id="cadastrar" name="cadastrar" >Cadastrar</button>
                <?php
                else:
                ?>
                <button onclick="validaUserAtualizado(); return false;" id="atualizar" name="atualizar" >Atualizar</button>
                <button onclick="apagarUser(); return false;" id="apagar" name="apagar" >Excluir conta</button>
                <?php
                endif;
                ?>
            </form>
        <div>
    </body>
</html>
