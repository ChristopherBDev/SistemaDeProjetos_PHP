<?php

require_once __DIR__.'/classes/Professor.class.php';
require_once __DIR__.'/classes/Aluno.class.php';
require_once __DIR__.'/classes/controllers/userController.class.php';

if (!isset($_POST)) {
    throw new Exception("Nada passado por Post.", 1);
    die;
}

$user = $_POST['User'];
$acao = $_POST['Acao'];

$userController = new UserController();

$response = array();

try {
    if ($user['tipo'] == 'professor') {
        $professor = new Professor();
        $professor->setNomeCompleto($user['nomeCompleto']);
        $professor->setEmail($user['email']);
        $professor->setSenha($user['senha']);
        $professor->setSIAPE($user['SIAPE']);
        
        if ($acao == 'Cadastrar') {
            $criou = $userController->create($professor);
        
            if ($criou) {
                $response[0] = "True";
                $response[1] = "Usuário Professor Inserido com sucesso!";
                echo json_encode($response);
            } else {
                $response[0] = "False";
                $response[1] = "Erro ao tentar inserir usuário.";
                echo json_encode($response);
            }
        } else {
            $professor->setUserID($user['userID']);
            $criou = $userController->update($professor);
        
            if ($criou) {
                $response[0] = "True";
                $response[1] = "Usuário Professor Atualizado com sucesso!";
                
                session_start();
                if (isset($_SESSION['user'])) {
                    $_SESSION['user']['nomeCompleto'] = $user['nomeCompleto'];
                    $_SESSION['user']['email'] = $user['email'];
                    $_SESSION['user']['senha'] = $user['senha'];
                }
                echo json_encode($response);
            } else {
                $response[0] = "False";
                $response[1] = "Erro ao tentar atualizar usuário.";
                echo json_encode($response);
            }
        }
    } else {
        $aluno = new Aluno();
        $aluno->setNomeCompleto($user['nomeCompleto']);
        $aluno->setEmail($user['email']);
        $aluno->setSenha($user['senha']);
        $aluno->setNumeroMaticula($user['numeroMatricula']);
        
        if ($acao == 'Cadastrar') {
            $criou = $userController->create($aluno);

            if ($criou) {
                $response[0] = "True";
                $response[1] = "Usuário Aluno inserido com sucesso!";
                echo json_encode($response);
            } else {
                $response[0] = "False";
                $response[1] = "Erro ao tentar inserir usuário.";
                echo json_encode($response);
            }
        } else {
            $aluno->setUserID($user['userID']);
            $criou = $userController->update($aluno);

            if ($criou) {
                $response[0] = "True";
                $response[1] = "Usuário Aluno atualizado com sucesso!";
                
                session_start();
                if (isset($_SESSION['user'])) {
                    $_SESSION['user']['nomeCompleto'] = $user['nomeCompleto'];
                    $_SESSION['user']['email'] = $user['email'];
                    $_SESSION['user']['senha'] = $user['senha'];
                }
                echo json_encode($response);
            } else {
                $response[0] = "False";
                $response[1] = "Erro ao tentar atualizar usuário.";
                echo json_encode($response);
            }
        }
    }
} catch (Exception $ex) {
    throw new Exception("Erro ao tentar criar user: $ex", 1);

    die;
}
